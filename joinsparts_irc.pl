# Xonotic rcon2irc plugin by packer licensed under GPL - joinsparts_irc.pl
# Place this file inside the same directory as rcon2irc.pl and add the full filename to the plugins.

sub out($$@);
 	[ irc => q{:([^ ]*)![^ ]* JOIN #(.*)} => sub {
 		my ($nick) = @_;
 		$nick = color_dpfix $nick;
 		out dp => 0, "rcon2irc_say_as \"IRC\"  \"^2$nick joined^3\"";
 		return 0;
 	} ],

	[ irc => q{:([^ ]*)![^ ]* (PART|QUIT|KICK).*} => sub {
 		my ($nick) = @_;
 		$nick = color_dpfix $nick;
 		out dp => 0, "rcon2irc_say_as \"IRC\" \"^1$nick left^3\"";
 		return 0;
 	} ],

